var ISSApiStream = require('./issApiStream');
var ISSModelStream = require('./issModelStream');
var ISSModel = require('./issModel');

/**
 *	A simple class that makes it easier to launch our various methods
 *  given the passed arguments
 *	@param {object} args - The command line arguments
 */
var ISSRunner = function(args){

    // Declaring an empty object if nothing was passed
    args = args || {};

    // Our default options that could be overwritten
    this.options = {
        'id':           null,
        'rate':         null,
        'trackSpeed':   false
    };

    // Only continue if all of our options are good to go
    if( !this.setArgs(args) )
        return false;

    // Are we tracking our speed?
    if( this.options['trackSpeed'] ) {
        this.issApiStream = new ISSModelStream(this.options['id'], this.options['rate']);
        this.issApiStream.on('data', this.onSpeedStream.bind(this));
        return;
    }

    // The empty var that will hold our ISS model
    this.iss;

    // We're just streaming basic ISS data
    this.issApiStream = new ISSApiStream(this.options['id'], this.options['rate']);
    this.issApiStream.on('data', this.onDataStream.bind(this));
};

/**
 *	Sets this instance's options based on the passed arguments
 *	@param {object} args - The object of args to set as options
 */
ISSRunner.prototype.setArgs = function(args) {
    // Setting our possible arguments if were in the passed object
    if( args['id'] )
        this.options['id'] = args['id'];
    if( args['rate'] )
        this.options['rate'] = args['rate'];

    // Ensuring an ID was set
    if( !this.options['id'] ) {
        console.log("\nYou must set a value for --id. Here are your choices:");

        ISSModel.getList().then(function(list){
            for( var x in list )
                console.log("- " + list[x]['id']);
            console.log('');
        });

        return false;
    }

    // Ensuring a rate was set
    if( !this.options['rate'] ) {
        console.log("You must set a value for --rate (in milliseconds)");
        return false;
    }

    // Set trackSpeed to true if it was present
    if( args['trackSpeed'] )
        this.options['trackSpeed'] = true;

    return true;
};

/**
 *	Is run after the ISSApiStream receives data
 *	@param {object} json - The JSON data that gets returned from the API
 */
ISSRunner.prototype.onDataStream = function(json){
    // Was there an error?
    if( json['error'] ) {
        console.log(body['error']);
        process.exit();
    }

    // Declar our ISS Model if we haven't already
    if( !this.iss )
        this.iss = new ISSModel(json);

    // Otherwise update the model with our latest location
    else
        this.iss.updateLocation(json['latitude'], json['longitude'], json['velocity']);

    // Making a new line here because it's prettier
    console.log('');

    // We're just wanting to spit out some basic info
    console.log(this.iss.locationString(true));
    console.log(this.iss.googleMapsUrl());
};

ISSRunner.prototype.onSpeedStream = function(iss){

    // Get our speed
    var speed = iss.speedPerSecond();

    // The method returns null of it doesn't have enough data to
    // do the calucation
    if( !speed ) {
        console.log('...waiting for more data');
        return;
    }

    // Rounding to a decimal
    var speed = Math.round(speed * 10) / 10

    // Output the info
    console.log("Traveling at " + speed + " kilometers per second");
};

// Processing our arguments
var argv = require('minimist')(process.argv.slice(2));

// Instantiating our runner
new ISSRunner(argv);
