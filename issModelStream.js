var Readable = require('stream').Readable
var ISSApiStream = require('./issApiStream');
var ISSModel = require('./issModel');
var util = require('util');

/**
 *	Opens a Readable Stream that pulls from ISSApiStream
 *  and converts its data to an ISSModel.
 *	@param {int} id - The ID of the ISS object we want to track
 *	@param {int} minDelay - The delay in milliseconds between each reach
 */
var ISSModelStream = function(id, minDelay){

    // Instantiate our main stream
    this.mainStream = new ISSApiStream(id, minDelay);

    // Will hold our ISS Model after our first data event
    this.iss;

    // Initiating our stream
    Readable.call(this, {
        objectMode: true
    });
};

util.inherits(ISSModelStream, Readable);

/**
 *	The primary read method
 */
ISSModelStream.prototype._read = function(){

    // Only continue if we're allowed to read again
    if( !this.mainStream.canReadAgain() ) {
        this.mainStream.delayProcess();
        return;
    }

    // Initiate our main stream to pull in all of this
    // ISS object's data
    this.mainStream.on('data', function(json){

        // We haven't initiated our ISS object
        if( !this.iss )
            this.iss = new ISSModel(json);

        // Update the location of our ISS model
        else
            this.iss.updateLocation(json['latitude'], json['longitude'], json['velocity']);

        // Push the ISS object to our
        this.push(this.iss);

    }.bind(this))
};


module.exports = ISSModelStream;
