/*
    SUMMARY:
    It appears this collection of functions is intended to collect
    a batch of methods, and can execute either asyncronously or
    one at a time based on if sync gets passed.


    BUGS:
    I came across three pieces of code that appear to have a bug.
    I've denoted each line with a "//BUG" comment above it.

    1) Within the add() method, there is an anonymous method called
        queue() which attemps to use a variable called 'arguments'.
        This must be a bug as `arguments` is never declared.

    2) In the requestAnimationFrame() method, the first few lines
        attempt to find the current browser's requestAnimationFrame
        method under the `global` object. The method will likely be
        under the `window` object.

    3) In the Batch.queue() method, the module generates a new
        animation frame, but is NOT executing the job. That method
        might be better used in the Batch.run() method's for loop
        where it actually executes the jobs.


    CONCLUSION:
    There are several things I would do differently were I writing
    this module:

    1) I would declare the object's methods within the object instead
        of making them anonymous functions and THEN assigning them as
        prototypes

    2) I would move Batch.queue() to a "private" method by changing
        it to Batch._queue(). You can see that Batch.add() uses this
        method by first assigning it a certain object scope and
        putting it through Batch.add()'s run() method. It seems like
        in the current structure it'd be very easy to misuse Batch.queue()
        when really all new jobs should be run through the Batch.add()
        method.

    3) I would remove the unnecessary brackets from the few if statements
        and the single for-loop that just have a single line under them.

    I've added comments below that outline what the code is doing to the
    best of my understanding.
*/



module.exports = Batch

/**
 *	A module that queues and runs a collection
 *  of jobs, likely involving JS or CSS animations.
 *	@param {bool} sync - Whether or not to perform these jobs at once or in order
 */
function Batch(sync) {

    // Ensuring this is a Batch object
    if(!(this instanceof Batch)) {
        return new Batch(sync)
    }

    // Setting up our initial variables
    this.jobs = []
    this.sync = sync
    this.frame = null
    this.run = this.run.bind(this)
}

// Assigning our anonymous functions to be prototypes of Batch
Batch.prototype.request_frame = request_frame
Batch.prototype.queue = queue
Batch.prototype.add = add
Batch.prototype.run = run

/**
 *	Adds a new job to our quete
 *	@param {function} fn - The actions to perform for this job
 */
function add(fn) {
    // Declaring our variables
    var queued = false
    , batch = this
    , self
    , args

    return queue

    // An anonymous function that queues this job
    function queue() {

        // BUG: arguments isn't a variable
        args = [].slice.call(arguments)

        // Preserving the object's current scope
        self = this

        // Add this to fn to the class's array of jobs
        if(!queued) {
            queued = true
            batch.queue(run)
        }
    }

    // An anonymous method that runs this function
    function run() {

        // Mark this job as no longer queued
        queued = false

        // Run this function with the correct context
        fn.apply(self, args)
    }
}

/**
 *	Appends a fn to the array of jobs
 *	@param {function} fn - The actions to perform for this job
 */
function queue(fn) {
    // Push the fn to the jobs array
    this.jobs.push(fn)

    // If we're not syncing then request a timed frame
    // to complete the job
    // BUG: The request frame should be generated at the
    // moment the job is executed and not when it gets
    // added to the list of jobs to perform
    if(!this.sync) {
        this.request_frame()
    }
}

/**
 *	Executes all of the current jobs
 */
function run() {

    // Saving our jobs to a temp var
    var jobs = this.jobs

    // Emptying our class jobs array
    this.jobs = []

    // Reseting our flushing any timed events we had
    // set previously
    this.frame = null

    // Loop through and execute each job
    for(var i = 0, l = jobs.length; i < l; ++i) {
        jobs[i]()
    }
}

/**
 *	Returns a new animation frame if one isn't
 *  already set
 */
function request_frame() {
    // Don't return anything if we've already
    // got a frame
    if(this.frame) {
        return
    }

    // Set the frame
    this.frame = requestAnimationFrame(this.run)
}

/**
 *	Generates a new animation frame and passes
 *  the job fn as the callback
 *	@param {function} callback - The fn to perform for this frame
 */
function requestAnimationFrame(callback) {

    // Request our animation frame depending on which browser
    // we're in. Fallback on a timeout if nothing else
    // BUG: these should be looking for the raf method under 'window' and not 'global'
    var raf = global.requestAnimationFrame ||
    global.webkitRequestAnimationFrame ||
    global.mozRequestAnimationFrame ||
    timeout

    // Requesting our frame
    return raf(callback)

    // A simple timeout if not other requestAnimationFrame
    // functions were found
    function timeout(callback) {
        return setTimeout(callback, 1000 / 60)
    }
}
