var Readable = require('stream').Readable;
var util = require('util');
var request = require('request');

/**
 *	Opens a Readable Stream that pulls in JSON
 *  data on the passed ISS ID.
 *	@param {int} id - The ID of the ISS object we want to track
 *	@param {int} minDelay - The delay in milliseconds between each reach
 */
var ISSApiStream = function(id, minDelay){

    // Ensure we were passed our params
    if( !id || !minDelay )
        throw "An ID and minDelay are required!";

    // The ID of our satellite
    this.id = id;

    // The minimum delay between reads
    this.minDelay = minDelay;

    // The URL we're going to hit
    this.url = "https://api.wheretheiss.at/v1/satellites/" + this.id

    // The date / time of the call read we perform
    this._lastRead = null;

    // The number of times we've made a read this second
    this._totalReads = 0;

    // Initiate our stream
    Readable.call(this, {
        objectMode: true
    });
};

util.inherits(ISSApiStream, Readable);

/**
 *	Determines whether or not enough time has lapsed
 *  for us to make another read.
 */
ISSApiStream.prototype.canReadAgain = function(){

    // If _lastRead is null it means it's our first
    // time through, so always return true
    if( this._lastRead === null )
        return true;

    // Calculating how many milliseconds have passed
    var currentMsDelay = new Date() - this._lastRead;

    // Don't read if our current delay is less than minDelay
    if( currentMsDelay <= this.minDelay )
        return false;

    // If we've made it this far then it all checks out
    return true;
};

/**
 *	Pushes the current read method onto the next process,
 *  used when we need a delay.
 */
ISSApiStream.prototype.delayProcess = function(){
    return process.nextTick(function(){
        this._read();
    }.bind(this));
};

/**
 *	The primary read method.
 */
ISSApiStream.prototype._read = function(){

    // Delay the read to the next process if we
    // aren't allowed to read again
    if( !this.canReadAgain() )
        return this.delayProcess();

    // Pulling the data from the API
    var resp = request.get(this.url, function(err, resp, body){

        // Stop everything if we encountered an error
        if( err ) {
            console.error(err);
            process.exit();
        }

        // Attempt to parse the body string into JSON
        try {
            var json = JSON.parse(body);

        } catch(e) {
            console.error("\nThere was a problem parsing JSON:\n" + e + "\n")
            process.exit();
        }

        // Reset our _lastRead to right now
        this._lastRead = new Date();

        // Push our data
        this.push(json);

    }.bind(this));
};


module.exports = ISSApiStream;
