var sinon = require('sinon');
var request = require('request');
var ISSApiStream = require('../issApiStream');

describe("The ISS Stream", function(){

    var iss;
    var requestStub;

    beforeEach(function(){
        iss = new ISSApiStream(25544, 2000);

        var bodyString = '{"id":25544,"name":"iss","latitude":8.0099004685149,'+
            '"longitude":106.07477519589,"altitude":409.64714974975,'+
            '"velocity":27592.476073302,"visibility":"daylight","footprint":4454.4132367663,'+
            '"timestamp":1474672700,"daynum":2457655.4710648,"solar_lat":-0.53865409584537,'+
            '"solar_lon":188.42915503037,"units":"kilometers"}';

        requestStub = sinon.stub(request, 'get').yields(null, {}, bodyString);
    });

    afterEach(function(){
        request.get.restore();
    });

    describe("on instantiation", function(){

        it("should throw an error if no args were passed", function(){
            // When nothing is passed
            expect(function(){
                new ISSApiStream();
            }).toThrow("An ID and minDelay are required!");

            // When only an ID is passed
            expect(function(){
                new ISSApiStream(123);
            }).toThrow("An ID and minDelay are required!");

            // When only a rate is passed
            expect(function(){
                new ISSApiStream(null, 2000);
            }).toThrow("An ID and minDelay are required!");
        });

        it("should have expected default values", function(){
            expect(iss.id).toEqual(25544);
            expect(iss.minDelay).toEqual(2000);
            expect(iss._lastRead).toBe(null);
            expect(iss._totalReads).toEqual(0);
        });

    });

    describe("should have a canReadAgain() method", function(){

        it("that allows a read on instantiation", function(){
            expect(iss.canReadAgain()).toEqual(true);
        });

        it("that does not allow a read until after the passed delay", function(done){
            // Setting our _lastRead value to now
            iss._lastRead = new Date();

            // We should not be able to read now
            expect(iss.canReadAgain()).toEqual(false);

            // We should still be unable to read after 1 second
            setTimeout(function(){
                expect(iss.canReadAgain()).toEqual(false);
            }, 1000);

            // We should be able to read after a 2 second delay
            setTimeout(function(){
                expect(iss.canReadAgain()).toEqual(true);
                done();
            }, 2000);
        });

    });

    describe("should have a _read() method", function(){

        it("that's not allowed to be called more than once within the passed rate limit", function(){
            // Our delay process method
            var delayStub = sinon.stub(iss, 'delayProcess');

            // Calling an initial read
            iss._read();

            // We should not have been rate limited
            expect(delayStub.callCount).toEqual(0);

            // Calling another read immediately
            iss._read();

            // We should have been rate limited
            expect(delayStub.callCount).toEqual(1);
        });
    });
});
