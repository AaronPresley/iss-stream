var sinon = require('sinon');
var request = require('request');
var ISSModelStream = require('../issModelStream');

describe("The ISS Model Stream", function(){

    var iss;
    var requestStub;

    beforeEach(function(){
        iss = new ISSModelStream(25544, 2000);

        var bodyString = '{"id":25544,"name":"iss","latitude":8.0099004685149,'+
            '"longitude":106.07477519589,"altitude":409.64714974975,'+
            '"velocity":27592.476073302,"visibility":"daylight","footprint":4454.4132367663,'+
            '"timestamp":1474672700,"daynum":2457655.4710648,"solar_lat":-0.53865409584537,'+
            '"solar_lon":188.42915503037,"units":"kilometers"}';

        requestStub = sinon.stub(request, 'get').yields(null, {}, bodyString);
    });

    afterEach(function(){
        request.get.restore();
    });

    describe("on instantiation", function(){

        it("should throw an error if no args were passed", function(){
            // When nothing is passed
            expect(function(){
                new ISSModelStream();
            }).toThrow("An ID and minDelay are required!");

            // When only an ID is passed
            expect(function(){
                new ISSModelStream(123);
            }).toThrow("An ID and minDelay are required!");

            // When only a rate is passed
            expect(function(){
                new ISSModelStream(null, 2000);
            }).toThrow("An ID and minDelay are required!");
        });

    });
});
