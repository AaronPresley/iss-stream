var sinon = require('sinon');
var request = require('request');
var ISSModel = require('../issModel');


describe("The ISS Model", function(){
    // Declaring the ISS object we'll be using
    var thisIss;

    beforeEach(function(){
        // Instantiating an example ISS object
        thisIss = new ISSModel({
            id: 25544,
            name: 'iss',
            latitude: 8.0099004685149,
            longitude: 106.07477519589,
            altitude: 409.64714974975,
            velocity: 27592.476073302,
            visibility: 'daylight',
            footprint: 4454.4132367663,
            timestamp: 1474672700,
            daynum: 2457655.4710648,
            solar_lat: -0.53865409584537,
            solar_lon: 188.42915503037,
            units: 'kilometers'
        });

    });

    afterEach(function(){
    });

    it("should require an ID on instantiation", function(){
        expect(function(){
            new ISSModel();
        }).toThrow("You must set an ID!");
    });

    it("should require a lat / lng on instantiation", function(){

        // Not setting either
        expect(function(){
            new ISSModel({
                id: 1
            });

        }).toThrow("You must set a latitude and longitude!");

        // Only setting longitude
        expect(function(){
            new ISSModel({
                id: 1,
                longitude: 8.123
            });

        }).toThrow("You must set a latitude and longitude!");

        // Only setting latitude
        expect(function(){
            new ISSModel({
                id: 1,
                latitude: 106.123
            });

        }).toThrow("You must set a latitude and longitude!");
    });

    it("should save the initial location in pastLocation", function(){

        expect(thisIss.pastLocations.length).toEqual(1);

        // Ensure the time was recoded
        expect(thisIss.pastLocations[0]['timeRecorded'] instanceof Date).toBe(true);

        // Ensure the lat / lng were recoded correctly
        expect(thisIss.pastLocations[0]['latitude']).toEqual(8.0099004685149);
        expect(thisIss.pastLocations[0]['longitude']).toEqual(106.07477519589);
    });

    it("should append to pastLocations after running updateLocation", function(){

        // Updating our location
        thisIss.updateLocation(8.1234, 106.1234);

        expect(thisIss.pastLocations.length).toEqual(2);

        // Ensure the time was recoded
        expect(thisIss.pastLocations[0]['timeRecorded'] instanceof Date).toBe(true);

        // Ensure the lat / lng were recoded correctly
        expect(thisIss.pastLocations[0]['latitude']).toEqual(8.0099004685149);
        expect(thisIss.pastLocations[0]['longitude']).toEqual(106.07477519589);
    });

    it("should output a location string with default args", function(){
        var expectedString = "8.0099004685149, 106.07477519589";
        expect(thisIss.locationString()).toEqual(expectedString);
    });

    it("should output a location string with passed args", function(){
        var expectedString = "Satellite #25544 is currently at 8.0099004685149, 106.07477519589";
        expect(thisIss.locationString(true)).toEqual(expectedString);
    });

    it("should output an accurate Google Maps URL with default args", function(){
        var expectedUrl = "https://www.google.com/maps?t=k&z=5&q=8.0099004685149,106.07477519589&ll=8.0099004685149,106.07477519589";
        expect(thisIss.googleMapsUrl()).toEqual(expectedUrl);
    });

    it("should output an accurate Google Maps URL with passed args", function(){
        var expectedUrl = "https://www.google.com/maps?t=m&z=2&q=123,123&ll=123,123";

        // Some settings to overwrite the defaults
        var location = {'latitude':123, 'longitude':123};
        var zoom = 2;
        var type = 'm';

        expect(thisIss.googleMapsUrl(zoom, type, location)).toEqual(expectedUrl);
    });

    it("should be able to calculate the speed per second", function(){
        // Setting our location history to be recoded 10 seconds apart
        // and the lat / lng for Apex and Baerlic
        thisIss.pastLocations = [
            {'timeRecorded': new Date(2016, 0, 1, 0, 0, 0),
                'latitude': 45.5067561,'longitude': -122.6565966},
            {'timeRecorded': new Date(2016, 0, 1, 0, 0, 10),
                'latitude': 45.5047373,'longitude': -122.6544936}
        ];

        expect(thisIss.speedPerSecond()).toBe(0.02779388626981235);
        expect(thisIss.speedPerSecond('m')).toBe(0.017270320248382164);

    });

    it("should be able to calculate the number of seconds between two date objects", function(){
        var date1 = new Date(2016, 0, 1, 0, 0, 10);
        var date2 = new Date(2016, 0, 1, 0, 0, 0);
        expect(thisIss._calculateSecondsBetween(date1, date2)).toEqual(10);
    });

    it("should be able to calculate the distance between two lat / lng objects", function(){
        var baerlicLoc = {'latitude': 45.5067561,'longitude': -122.6565966};
        var apexLoc = {'latitude': 45.5047373,'longitude': -122.6544936};
        var expectedDistance = 0.2779388626981235;
        expect(thisIss._calcualteKmBetween(baerlicLoc, apexLoc)).toEqual(expectedDistance);
    });

    it("should return a list of all current satellites", function(done){

        // Stubbing our request.get method and telling it to
        // respond with expected data
        sinon.stub(request, 'get').yields(null, {}, '[{"id":123,"name":"Some Satellite"}]');

        ISSModel.getList().then(function(list){
            expect(list).toEqual([{
                id: 123,
                name: "Some Satellite"
            }]);
            done();

        }).catch(function(err){
            fail(err);
            done();

        });

        request.get.restore();
    });
});
