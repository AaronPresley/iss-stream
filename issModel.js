var Q = require('q');
var request = require('request');

/**
 *	Represents a single ISS object
 *	@param {object} data - The JSON data pulled from whereiss.at
 */
var ISSModel = function(data){

    // Defauting to an empty object
    data = data || {};

    // Pulling in all of our information from the JSON object
    this.id = data['id'];
    this.name = data['name'];
    this.latitude = data['latitude'];
    this.longitude = data['longitude'];
    this.altitude = data['altitude'];
    this.velocity = data['velocity'];
    this.visibility = data['visibility'];
    this.footprint = data['footprint'];
    this.timestamp = data['timestamp'];
    this.daynum = data['daynum'];
    this.solarLatitude = data['solar_lat'];
    this.solarLongitude = data['solar_lon'];
    this.units = data['units'];

    // Ensure we set an ID
    if( !this.id )
        throw "You must set an ID!"

    // Ensure latitude and longitude are set
    if( !this.latitude || !this.longitude )
        throw "You must set a latitude and longitude!"

    // An array that will hold all of our locations as
    // they get updated
    this.pastLocations = []

    // Setting our first location
    this.updateLocation(this.latitude, this.longitude);
};


/**
 *	Used to update this ISS' current location
 *	@param {float} latitude - The current latitude
 *	@param {float} longitude - The current longitude
 */
ISSModel.prototype.updateLocation = function(latitude, longitude, velocity) {
    // Updating our curent position
    this.latitude = latitude;
    this.longitude = longitude;
    this.velocity = velocity

    // Adding this to our location history
    this.pastLocations.push({
        'timeRecorded': new Date(),
        'latitude': this.latitude,
        'longitude': this.longitude,
        'velocity': this.velocity
    });
};


/**
 *	Returns the current lat / lon coordinates as a string output
 *	@param {bool} friendly - Whether or not to make the text friendly
 */
ISSModel.prototype.locationString = function(friendly){

    // Defaulting to not being friendly
    friendly = friendly || false;

    // Our lat / lng output that'll be used no matter what
    var latLngString = this.latitude + ", " + this.longitude;

    // Should we output a full sentence?
    if( friendly )
        return "Satellite #" + this.id + " is currently at " + latLngString;

    // Output the barebones info
    return latLngString;
};


/**
 *	Generates a Google Maps URL based on this ISS' current location.
 *	@param {int} zoom - The zoom level you want the map to be
 *	@param {string} type - The type of map you want the link to be
 *	@param {object} location - The lat / long to use for the map
 */
ISSModel.prototype.googleMapsUrl = function(zoom, type, location){

    // Defaulting to our most recent location
    location = location || this.pastLocations[this.pastLocations.length-1];

    // Making our default zoom 5
    zoom = zoom || 5;

    // Making our default type k, for satellite
    type = type || 'k'

    // Returning the merged string
    return "https://www.google.com/maps" +
        "?t=" + type +
        "&z=" + zoom +
        "&q=" + location['latitude'] + "," + location['longitude'] +
        "&ll=" + location['latitude'] + "," + location['longitude'];
};


/**
 *	Calculates the current speed per second that this ISS
 *  object is traveling.
 *	@param {string} unit - The unit of measure to use, defaults to kilometers
 */
ISSModel.prototype.speedPerSecond = function(unit){

    // Default to kilometers
    unit = unit || 'kilometers';

    // We can't do anything with just one lat / lng so
    // return the empty dict
    if( this.pastLocations.length <= 1 )
        return null;

    // Pulling our first and last location recorded
    var firstLoc = this.pastLocations[this.pastLocations.length - 1];
    var lastLoc = this.pastLocations[0];

    // How many seconds have passed since we first started recorded?
    var totalSeconds = this._calculateSecondsBetween(firstLoc['timeRecorded'], lastLoc['timeRecorded']);

    // How much distance has passed?
    var totalDistance = this._calcualteKmBetween(firstLoc, lastLoc);

    // What's our speed per second in kilometers?
    var sps = totalDistance / totalSeconds;

    switch(unit){
        case 'kilometers':
        case 'k':
            // Don't do any conversion
            return sps;
        break;

        case 'miles':
        case 'm':
            // Convert from kilometers to miles
            return sps / 1.609344
        break;

        default:
            throw "I don't understand the unit of \"" + unit + "\"";
        break;
    }
};

/**
 *	Calculates how many seconds of lapsed between two
 *  different Date objects
 *	@param {Date} time1 - The most first Date object to use
 *	@param {Date} time2 - The most recent Date object to use
 */
ISSModel.prototype._calculateSecondsBetween = function(time1, time2){
    return (time1.getTime() - time2.getTime()) / 1000;
};

/**
 *	Calculates the distance in km between two different
 *  location objects from this.pastLocations
 *	@param {object} location1 - The first location to use
 *	@param {object} location2 - The second location to use
 */
ISSModel.prototype._calcualteKmBetween = function(location1, location2){

    // Setting all of our variables
    var lat1 = location1['latitude'];
    var lon1 = location1['longitude'];
    var lat2 = location2['latitude'];
    var lon2 = location2['longitude'];

    // Equasion pulled from http://stackoverflow.com/a/21623206/360847
    var p = 0.017453292519943295;
    var c = Math.cos;
    var a = 0.5 - c((lat2 - lat1) * p)/2 +
        c(lat1 * p) * c(lat2 * p) *
        (1 - c((lon2 - lon1) * p))/2;
    var d = 12742 * Math.asin(Math.sqrt(a));

    // Return our final distance
    return d;
};

/**
 *	A static method that pulls a list of all the
 *  current ISS objects.
 */
ISSModel.getList = function(){
    var deferred = Q.defer();

    // Hit the actual API
    request.get("https://api.wheretheiss.at/v1/satellites", function(err, resp, body){
        // There was a problem
        if( err )
            deferred.reject(err);

        else {
            // Attempt to parse the JSON
            try {
                var json = JSON.parse(body);
            } catch(e) {
                deferred.reject(e);
            }

            deferred.resolve(json);
        }
    });

    return deferred.promise;
};

module.exports = ISSModel;
