# Notes

This was my first exercise in heavily using node's `Readable` stream. I
admit some of my understanding is lacking when working with the API, but
I've done the best I could given the short timeframe.

## Improvements

I have nearly full test coverage, however I really struggled to figure
out the best way to test an active stream. I split my logic into different
methods and tested as much of the streams as I could. The `ISSModel` is
fully tested.

## Final Thoughts

This was a fun exercise. I learned a ton using the `Readable` class,
and added some extra flair to my output (like generating a Google Maps
link for where the ISS is at that moment).

For the 2nd challenge, the instructions were:

> Implement a stream that transforms the location stream into change in
lat/lng per second.

I wasn't entirely sure what was meant by "the change in lat/lng per second",
but I assume it meant the overall speed per second so that's what I wrote
the script to do. Sorry if that was the wrong interpretation :(

Thank you for the opportunity.

\- Aaron

---


# ISS Streams

This project is a collection of objects that ready from the [wheretheiss.at](http://wheretheiss.at) API.


## Installation

In this directory, install the npm requirements by running:

    npm install


## Testing

Run tests with `npm test`:

    $ npm test

    > iss-stream@0.1.0 test /Users/AaronPresley/Development/UAChallenges
    > jasmine

    Started
    ..................


    18 specs, 0 failures
    Finished in 2.035 seconds

## Usage

### Arguments

| Command               | Description
|--                     |--
| id                    | The ID of the ISS object you want to track
| rate                  | The rate in milliseconds to read from the API
| trackSpeed            | When passed, it will output the current kilometers per second the given ISS is traveling

### Listing ISS ID's

To see a list of all ISS objects you can enter, run `node index.js` without
any arguments:

    $ node index.js

    You must set a value for --id. Here are your choices:
    - 25544


### Streaming ISS Location

You can stream the current location of an ISS object by passing the `id` and the `rate` arguments:

    $ node index.js --id=ISS_ID --rate=RATE_IN_MS

    Satellite #25544 is currently at -42.026126279079, 96.434080515591
    https://www.google.com/maps?t=k&z=5&q=-42.026126279079,96.434080515591&ll=-42.026126279079,96.434080515591

    Satellite #25544 is currently at -42.168765226545, 96.707045285962
    https://www.google.com/maps?t=k&z=5&q=-42.168765226545,96.707045285962&ll=-42.168765226545,96.707045285962

You can exit the stream by hitting `^C`;

### Streaming ISS Speed

You can stream the current speed of an ISS object by passing the `trackSpeed` object along with `id` and `rate`. Once the stream has several location coordinates it will begin calculating the ISS' speed.

    $ node index.js --id=ISS_ID --rate=RATE_IN_MS --trackSpeed
    ...waiting for more data
    Traveling at 6.2 kilometers per second
    Traveling at 5.3 kilometers per second

You can exit the stream by hitting `^C`;


# Batch Documentation

To see my solution to the 3rd challenge, look at the `batch.js` file in this repo. Or you
can [click here](https://bitbucket.org/AaronPresley/iss-stream/src/master/batch.js).
